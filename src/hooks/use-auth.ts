import { ref } from "vue";
import { name } from "../../package.json";
import { clearToken, setToken, userApi } from "../api";
import { User } from "../api/user";

export const TOKEN_KEY = `${name}.token`;

const user = ref<User | null>(null);

async function signIn(username: string, password: string) {
  try {
    const { token } = await userApi.signIn(username, password);

    if (token) {
      localStorage.setItem(TOKEN_KEY, token);
      setToken(token);
      const currentUser = await userApi.getMe();
      user.value = currentUser;
    }
  } catch (err) {
    console.error(err);
    throw err;
  }
}

function signOut() {
  localStorage.removeItem(TOKEN_KEY);
  user.value = null;
  clearToken();
}

async function checkAuth() {
  const token = localStorage.getItem(TOKEN_KEY);

  if (token) {
    try {
      setToken(token);
      const currentUser = await userApi.getMe();
      user.value = currentUser;

      return currentUser;
    } catch (error) {
      console.error(error);
    }
  }

  return null;
}

export default function useAuth() {
  return { user, signIn, signOut, checkAuth };
}
