import { createRouter, createWebHashHistory } from "vue-router";
import mainLayoutVue from "./layouts/main-layout.vue";
import ChatPage from "./pages/chat.vue";
import authLayoutVue from "./layouts/auth-layout.vue";
import signInPage from "./pages/sign-in.vue";
import useAuth from "./hooks/use-auth";
import UsersPage from "./pages/users.vue";

export const PAGES = {
  CHAT: "chat-page",
  USERS: "users-page",
  SIGN_IN: "sign-in-page",
};

export const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    {
      path: "/:pathMatch(.*)*",
      redirect: "/chat",
    },
    {
      path: "/chat",
      component: mainLayoutVue,
      meta: { isProtected: true },
      children: [
        {
          name: PAGES.CHAT,
          path: ":chatId*",
          component: ChatPage,
        },
      ],
    },
    {
      path: "/users",
      component: mainLayoutVue,
      meta: { isProtected: true },
      children: [
        {
          name: PAGES.USERS,
          path: ":userId*",
          component: UsersPage,
        },
      ],
    },
    {
      path: "/auth/sign-in",
      component: authLayoutVue,
      meta: { isPublicOnly: true },
      children: [
        {
          name: PAGES.SIGN_IN,
          path: "",
          component: signInPage,
        },
      ],
    },
  ],
});

router.beforeEach((to, _, next) => {
  const { user } = useAuth();

  if (to.meta.isProtected && !user.value) {
    return next({ name: PAGES.SIGN_IN });
  }
  if (to.meta.isPublicOnly && user.value) {
    return next({ name: PAGES.CHAT });
  }

  return next();
});
