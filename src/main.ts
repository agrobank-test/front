import { createApp } from "vue";
import AppVue from "./app.vue";
import { router } from "./router";
import useAuth from "./hooks/use-auth";
import { VueQueryPlugin } from "@tanstack/vue-query";
import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";

async function main() {
  const app = createApp(AppVue);

  const { checkAuth } = useAuth();

  await checkAuth();

  dayjs.extend(relativeTime);

  app.use(VueQueryPlugin);
  app.use(router);
  app.mount("#app");
}

main();
