import { appAxios } from ".";

type SignInResponse = {
  token: string;
};

export enum UserRole {
  ADMIN = "ADMIN",
  CUSTOMER = "CUSTOMER",
}

export type User = {
  id: string;
  username: string;
  name: string;
  role: UserRole;
  avatar: string;
};

export async function signIn(username: string, password: string) {
  const { data } = await appAxios.post<SignInResponse>("user/sign-in", {
    username,
    password,
  });
  return data;
}

export async function getMe() {
  const { data } = await appAxios.get<User>("user/me");
  return data;
}

export async function findAll() {
  const { data } = await appAxios.get<User[]>("user");
  return data;
}

export async function findById(userId: string) {
  const { data } = await appAxios.get<User>(`user/${userId}`);
  return data;
}

export type CreateUserPayload = {
  username: string;
  name: string;
  password: string;
  role: UserRole;
  avatar: string;
};
export type PatchUserPayload = Partial<Omit<CreateUserPayload, "role">>;

export async function patchById(userId: string, payload: PatchUserPayload) {
  const { data } = await appAxios.patch<User>(`user/${userId}`, payload);
  return data;
}

export async function deleteById(userId: string) {
  const { data } = await appAxios.delete<User>(`user/${userId}`);
  return data;
}

export async function create(payload: CreateUserPayload) {
  const { data } = await appAxios.post<User>(`user`, payload);
  return data;
}
