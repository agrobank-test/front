import { appAxios } from ".";

export type Chat = {
  id: string;
  userId: string;
  messages: Message[];
};

export type Message = {
  id: string;
  createdAt: string;
  text: string;
  senderId: string;
  chatId: string;
  sender: SenderName;
};

type SenderName = {
  name: string;
};

type Chats = Array<
  Chat & {
    user: UserNameAndAvatar;
    messages: MessageWithoutSender;
  }
>;

type MessageWithoutSender = {
  id: string;
  createdAt: string;
  text: string;
  senderId: string;
  chatId: string;
};

type UserNameAndAvatar = {
  name: string;
  avatar: string;
};

export async function my() {
  const { data } = await appAxios.get<Chat>("chat/my");
  return data;
}

export async function all() {
  const { data } = await appAxios.get<Chats>("chat/all");
  return data;
}

export async function sendMessage(text: string, chatId?: string) {
  const { data } = await appAxios.post<Message>("chat/send-message", {
    text,
    ...(chatId ? { chatId } : {}),
  });
  return data;
}

export async function findById(chatId: string) {
  const { data } = await appAxios.get<Chat>(`chat/${chatId}`);
  return data;
}
