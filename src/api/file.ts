import { appAxios } from ".";

type FileUploadResponse = {
  fileUrl: string;
};

export async function uploadFile(formData: FormData) {
  const { data } = await appAxios.post<FileUploadResponse>("file", formData);
  return data;
}
