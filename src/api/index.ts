import axios from "axios";

export const basePath =
  import.meta.env.VITE_BASE_PATH || "http://localhost:3000";
export const appAxios = axios.create({
  baseURL: `${basePath}/api`,
});

export * as userApi from "./user";
export * as chatApi from "./chat";
export * as fileApi from "./file";

export function setToken(token: string) {
  appAxios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
}

export function clearToken() {
  delete appAxios.defaults.headers.common["Authorization"];
}
