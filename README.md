# Agrobank chat front

## build docker image
`docker build --build-arg VITE_BASE_PATH=http://path-to-backend:1234 . -t chat-app`

## run docker image on port `:8080`
`docker run -p 8080:80 -t chat-app`